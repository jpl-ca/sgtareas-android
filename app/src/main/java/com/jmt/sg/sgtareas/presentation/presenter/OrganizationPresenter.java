package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgtareas.data.datasource.ValidateOrganizationDataSourceFactory;
import com.jmt.sg.sgtareas.data.repository.ValidateOrganizationDataRepository;
import com.jmt.sg.sgtareas.domain.interactor.OrganizationInteractor;
import com.jmt.sg.sgtareas.domain.repository.OrganizationRepository;
import com.jmt.sg.sgtareas.presentation.view.OrganizationView;

/**
 * Created by jmtech on 5/12/16.
 */
public class OrganizationPresenter implements Presenter<OrganizationView>,OrganizationInteractor.Callback {

    private Context context;
    private OrganizationView organizationView;
    private OrganizationInteractor organizationInteractor;

    @Override
    public void addView(OrganizationView view) {
        organizationView = view;
        context = view.getContext();

        OrganizationRepository organizationRepository = new ValidateOrganizationDataRepository(new ValidateOrganizationDataSourceFactory(context));
        organizationInteractor = new OrganizationInteractor(organizationRepository);

    }

    @Override
    public void removeView() {
        organizationView = null;
    }

    public void validateOrganization(String organization_name){
        organizationView.showLoading();
        organizationInteractor.validateOrganization(organization_name, this);
    }

    @Override
    public void onValidateSuccess() {
        organizationView.successValidate();
    }

    @Override
    public void onValidateError(String message) {
        organizationView.hideLoading();
        organizationView.showErrorMessage(message);
    }
}