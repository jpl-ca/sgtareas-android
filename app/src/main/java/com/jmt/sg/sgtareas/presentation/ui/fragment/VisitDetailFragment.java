package com.jmt.sg.sgtareas.presentation.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.domain.model.RouteVisitE;
import com.jmt.sg.sgtareas.domain.model.VisitCheckListE;
import com.jmt.sg.sgtareas.presentation.presenter.ConfirmDialogPresenter;
import com.jmt.sg.sgtareas.presentation.presenter.VisitDetailPresenter;
import com.jmt.sg.sgtareas.presentation.utils.Constants;
import com.jmt.sg.sgtareas.presentation.view.ConfirmDialogView;
import com.jmt.sg.sgtareas.presentation.view.VisitDetailView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitDetailFragment extends BaseFragment implements VisitDetailView{
    @BindString(R.string.t_detalle_visita) protected String titlebar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Bind(R.id.txt_cliente) TextView txt_cliente;
    @Bind(R.id.txt_telefono) TextView txt_telefono;
    @Bind(R.id.txt_direccion) TextView txt_direccion;
    @Bind(R.id.txt_referencia) TextView txt_referencia;
    @Bind(R.id.txt_tiempo) TextView txt_tiempo;
    @Bind(R.id.lv_checklist) LinearLayout lv_checklist;
    @Bind(R.id.tv_state_visit_point) TextView tv_state_visit_point;
    @Bind(R.id.btnSolicitarReprogramado) Button btnSolicitarReprogramado;

    private ConfirmDialogPresenter confirmDialogPresenter;

    LayoutInflater inflater;
    private Context context;
    private OnVisitDetailListener presenter;
    private CheckBox checkBoxList[] = new CheckBox[]{};


    private boolean boxWasCheckedProgrammatically = false;

    public static final String ROUTE_VISIT_DATA = "RouteVisitData";
    public VisitDetailPresenter visitDetailPresenter;

    public static VisitDetailFragment instance(){
        return new VisitDetailFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_visit_detail, container, false);
        ButterKnife.bind(this, rootView);
        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        this.context = getContext();

        visitDetailPresenter = new VisitDetailPresenter();
        visitDetailPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnVisitDetailListener) {
            presenter = (OnVisitDetailListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @OnClick(R.id.btn_goto_direcction)
    protected void goToDirection() {
        visitDetailPresenter.getMyLocation(new VisitDetailPresenter.onGetMyLocation() {
            @Override
            public void setMyLocation(LatLng latlng) {
                RouteVisitE visit = visitDetailPresenter.getRouteVisit();

                double MeLat = latlng.latitude;
                double MeLng = latlng.longitude;

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+MeLat+","+MeLng+"&daddr="+visit.getLat()+","+visit.getLng()));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.btnSolicitarReprogramado)
    protected void solicitarReprogramado() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {}
            @Override
            public void clickConfirm(String input) {
                visitDetailPresenter.rescheduleVisitPoint(visitDetailPresenter.getRouteVisit().getId(),input);
            }
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showInputDialog(context.getString(R.string.s_se_guardara_como_reprogramado));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void settingData(RouteVisitE routeVisitE) {
        txt_cliente.setText(routeVisitE.getName());
        txt_telefono.setText(routeVisitE.getPhone());
        txt_direccion.setText(routeVisitE.getAddress());
        txt_referencia.setText(routeVisitE.getReference());

        String hora = routeVisitE.getTime();
        txt_tiempo.setText(hora != null?hora:"00:00:00");

        addChecklist(routeVisitE.getChecklist());
        setVisitState(routeVisitE.getVisit_state_id());
    }

    @Override
    public void onChangeVisitStateSuccess(int new_state) {
        setVisitState(new_state);
    }

    @Override
    public void onCheckedSuccess() {
        updateStatus();
    }

    @Override
    public void onRequestError(String message) {
        showMessage(ll_root,message);
    }


    private void updateStatus() {
        ArrayList<VisitCheckListE> checklist = visitDetailPresenter.getCheckList();
        RouteVisitE routeVisit = visitDetailPresenter.getRouteVisit();

        for (VisitCheckListE check : checklist){
            if(check.getChecked()==Constants.CHECKED_LIST_STATE.Unchecked){
                visitDetailPresenter.changeVisitPointStateLocal(routeVisit.getId(), Constants.VISIT_STATE.Scheduled);
                return;
            }
        }
        visitDetailPresenter.changeVisitPointStateLocal(routeVisit.getId(), Constants.VISIT_STATE.Done);
    }

    private void addChecklist(ArrayList<VisitCheckListE> checklist) {
        int idx = 0;
        checkBoxList = new CheckBox[checklist.size()];
        for (VisitCheckListE check : checklist){
            setCheckList(idx++,check);
        }
    }

    private void setVisitState(int visitState) {
        if(visitState== Constants.VISIT_STATE.Done){
            tv_state_visit_point.setText(getString(R.string.state_realizado));
            btnSolicitarReprogramado.setVisibility(View.GONE);
        }else if(visitState==Constants.VISIT_STATE.Scheduled){
            tv_state_visit_point.setText(getString(R.string.state_programado));
            btnSolicitarReprogramado.setVisibility(View.VISIBLE);
            boolean isCheckedOne = false;

            for (int i=0;i<checkBoxList.length;i++){
                if(checkBoxList[i].isChecked())
                    isCheckedOne = true;
            }

            if(isCheckedOne)btnSolicitarReprogramado.setVisibility(View.GONE);
            else btnSolicitarReprogramado.setVisibility(View.VISIBLE);

        }else if(visitState==Constants.VISIT_STATE.ReScheduling){
            for (int i=0;i<checkBoxList.length;i++)checkBoxList[i].setEnabled(false);
            tv_state_visit_point.setText(getString(R.string.state_reprogramado));
            btnSolicitarReprogramado.setVisibility(View.GONE);
        }
        presenter.sendResultSuccess(visitDetailPresenter.getRouteVisit());
    }

    private void setCheckList(final int idx, final VisitCheckListE check) {
        View vv  = inflater.inflate(R.layout.lv_checklist, lv_checklist, false);
        final CheckBox cb_checked = ((CheckBox)vv.findViewById(R.id.cb_checked));
        checkBoxList[idx] = cb_checked;
        final TextView tv_descripcion = ((TextView)vv.findViewById(R.id.tv_descripcion));
        tv_descripcion.setText(check.getDescription());
        boolean checked = check.getChecked() == Constants.CHECKED_LIST_STATE.Checked;
        cb_checked.setChecked(checked);
        cb_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!boxWasCheckedProgrammatically){
                    if (b){
                        checkListDone(idx,check.getId(), cb_checked);
                    }else{
                        unCheckListDone(idx, check.getId(), cb_checked);
                    }
                }else{
                    System.out.println("Bad Idea...!!!");
                }
                boxWasCheckedProgrammatically = false;
            }
        });
        lv_checklist.addView(vv);
    }

    private void unCheckListDone(final int idx, final long checkListId, final CheckBox cb_checked) {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                visitDetailPresenter.updateCheckListState(idx,Constants.CHECKED_LIST_STATE.Unchecked);
                visitDetailPresenter.updateCheckListStatus(checkListId, Constants.CHECKED_LIST_STATE.Unchecked);
            }

            @Override
            public void clickConfirm(String input) {}

            @Override
            public void clickCancel() {
                boxWasCheckedProgrammatically = true;
                cb_checked.setChecked(true);
            }
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_confirma_se_tarea_no_fue_completado));
    }

    private void checkListDone(final int idx, final long checkListId, final CheckBox cb_checked) {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                visitDetailPresenter.updateCheckListState(idx,Constants.CHECKED_LIST_STATE.Checked);
                visitDetailPresenter.updateCheckListStatus(checkListId, Constants.CHECKED_LIST_STATE.Checked);
            }

            @Override
            public void clickConfirm(String input) {}

            @Override
            public void clickCancel() {
                boxWasCheckedProgrammatically = true;
                cb_checked.setChecked(false);
            }
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_confirma_se_realizo_la_tarea));
    }


    public interface OnVisitDetailListener{
        void sendResultSuccess(RouteVisitE routeVisit);
        void onBackPressed();
    }
}