package com.jmt.sg.sgtareas.data.datasource;

import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface RescheduleVisitDataSource {
    void rescheduleVisit(long visit_point_id, String comment, final RepositoryCallback repositoryCallback);
}