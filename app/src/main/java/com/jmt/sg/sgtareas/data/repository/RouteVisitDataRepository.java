package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.RouteVisitDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.RouteVisitDataSource;
import com.jmt.sg.sgtareas.data.mapper.RouteVisitDataMapper;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.data.model.RouteResponse;
import com.jmt.sg.sgtareas.domain.interactor.RouteVisitInteractor;
import com.jmt.sg.sgtareas.domain.model.RouteE;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.RouteVisitRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteVisitDataRepository implements RouteVisitRepository {
    private final RouteVisitDataSourceFactory routeVisitDataSourceFactory;
    private final RouteVisitDataMapper visitDataMapper;

    public RouteVisitDataRepository(RouteVisitDataSourceFactory routeVisitDataSourceFactory, RouteVisitDataMapper visitDataMapper) {
        this.routeVisitDataSourceFactory = routeVisitDataSourceFactory;
        this.visitDataMapper = visitDataMapper;
    }

    @Override
    public void getRouteVisit(final RouteVisitInteractor.Callback rouInteractorCallback) {
        RouteVisitDataSource routeVisitDataSource = routeVisitDataSourceFactory.create(DataSourceFactory.CLOUD);
        routeVisitDataSource.getRouteVisitData(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                RouteResponse routeR = (RouteResponse)object;
                RouteE route = visitDataMapper.transformRoute(routeR);
                rouInteractorCallback.onGetRouteSuccess(route);
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                rouInteractorCallback.onGetRouteError(message);
            }
        });
    }
}