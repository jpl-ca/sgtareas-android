package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgtareas.data.repository.SGInfoDataRepository;
import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.Trackeable;
import com.jmt.sg.sgtareas.data.repository.TrackerDataLoadRepository;
import com.jmt.sg.sgtareas.domain.interactor.SGInfoInteractor;
import com.jmt.sg.sgtareas.domain.interactor.TrackerLoadInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.TrackerLoadDataRepository;
import com.jmt.sg.sgtareas.presentation.view.TrackeableView;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackeablePresenter implements Presenter<TrackeableView>,TrackerLoadInteractor.Callback {

    Context context;
    TrackeableView trackeableView;
    private TrackerLoadInteractor trackerLoadInteractor;

    private SGInfoInteractor sgInfoInteractor;

    @Override
    public void addView(TrackeableView view) {
        trackeableView = view;
        context = view.getContext();
        TrackerLoadDataRepository trackerLoadDataRepository = new TrackerDataLoadRepository(new TrackeableDataSourceFactory(context), new TrackeableDataMapper());
        trackerLoadInteractor = new TrackerLoadInteractor(trackerLoadDataRepository);

        SGInfoDataRepository sgInfoDataRepository = new SGInfoDataRepository(new SGInfoDataSourceFactory(context));
        sgInfoInteractor = new SGInfoInteractor(sgInfoDataRepository);
    }

    @Override
    public void removeView() {
        trackeableView = null;
    }

    public void getTrackeableInfo(){
        if(sgInfoInteractor.getTrackableType() == Trackeable.VEHICLE)
             trackerLoadInteractor.loadVehicleData(this);
        else trackerLoadInteractor.loadAgentData(this);
    }

    @Override
    public void onLoadVehicleData(VehicleTrackeable vehicleTrackeable) {
        trackeableView.showData(vehicleTrackeable);
    }

    @Override
    public void onLoadAgentData(AgentTrackeable agentTrackeable) {
        trackeableView.showData(agentTrackeable);
    }

    @Override
    public void onLoadDataError(String message) {
        trackeableView.showMessage(message);
    }
}