package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.RouteGeolocationInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RouteGeolocationRepository {
    void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback);
}