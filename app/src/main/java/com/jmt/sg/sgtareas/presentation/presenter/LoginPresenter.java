package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgtareas.data.repository.SGInfoDataRepository;
import com.jmt.sg.sgtareas.data.datasource.LoginDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.Trackeable;
import com.jmt.sg.sgtareas.data.repository.LoginDataRepository;
import com.jmt.sg.sgtareas.data.repository.TrackerDataSaveRepository;
import com.jmt.sg.sgtareas.domain.interactor.LoginInteractor;
import com.jmt.sg.sgtareas.domain.interactor.SGInfoInteractor;
import com.jmt.sg.sgtareas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.LoginRepository;
import com.jmt.sg.sgtareas.domain.repository.TrackerSaveDataRepository;
import com.jmt.sg.sgtareas.presentation.view.LoginView;

/**
 * Created by jmtech on 5/12/16.
 */
public class LoginPresenter implements Presenter<LoginView>, LoginInteractor.Callback, TrackerSaveInteractor.Callback{

    Context context;
    LoginView loginView;
    private LoginInteractor loginInteractor;
    private TrackerSaveInteractor trackerSaveInteractor;
    private SGInfoInteractor sgInfoInteractor;

    @Override
    public void addView(LoginView view) {
        loginView = view;
        context = view.getContext();

        LoginRepository loginRepository = new LoginDataRepository(new LoginDataSourceFactory(context),new TrackeableDataMapper());
        loginInteractor = new LoginInteractor(loginRepository);

        TrackerSaveDataRepository trackerRepository = new TrackerDataSaveRepository(new TrackeableDataSourceFactory(context),new TrackeableDataMapper());
        trackerSaveInteractor = new TrackerSaveInteractor(trackerRepository);

        SGInfoDataRepository sgInfoDataRepository = new SGInfoDataRepository(new SGInfoDataSourceFactory(context));
        sgInfoInteractor = new SGInfoInteractor(sgInfoDataRepository);
    }

    @Override
    public void removeView() {
        loginView = null;
    }

    public void login(String code,String password){
        loginView.showLoading();
        if(code.contains("-"))
             loginInteractor.loginVehicle(code,password, this);
        else loginInteractor.loginAgent(code,password, this);
    }

    @Override
    public void onLoginSuccess(VehicleTrackeable vehicleTrackeable) {
        loginView.successLogin();

        loginView.showErrorMessage("Bienvenido-V: "+vehicleTrackeable.getBrand());

        sgInfoInteractor.saveTrackableType(Trackeable.VEHICLE);

        trackerSaveInteractor.saveTrackerData(vehicleTrackeable,this);
    }

    @Override
    public void onLoginSuccess(AgentTrackeable agentTrackeable) {
        loginView.hideLoading();
        loginView.successLogin();

        loginView.showErrorMessage("Bienvenido-A: "+agentTrackeable.getFirst_name());

        sgInfoInteractor.saveTrackableType(Trackeable.AGENT);

        trackerSaveInteractor.saveTrackerData(agentTrackeable,this);
    }

    @Override
    public void onLoginError(String message) {
        loginView.hideLoading();
        loginView.showErrorMessage(message);
    }

    @Override
    public void onTrackerDataSaved() {

    }

    @Override
    public void onTrackerDataSavedError(String mess) {

    }
}