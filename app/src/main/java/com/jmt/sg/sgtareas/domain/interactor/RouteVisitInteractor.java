package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.data.model.RouteResponse;
import com.jmt.sg.sgtareas.domain.model.RouteE;
import com.jmt.sg.sgtareas.domain.repository.RouteVisitRepository;

/**
 * Created by jmtech on 5/25/16.
 */
public class RouteVisitInteractor {
    private final RouteVisitRepository routeVisitRepository;

    public RouteVisitInteractor(RouteVisitRepository routeVisitRepository) {
        this.routeVisitRepository = routeVisitRepository;
    }

    public void getRouteVisit(final Callback callback) {
        routeVisitRepository.getRouteVisit(callback);
    }

    public interface Callback {
        void onGetRouteSuccess(RouteE route);
        void onGetRouteError(String message);
    }
}