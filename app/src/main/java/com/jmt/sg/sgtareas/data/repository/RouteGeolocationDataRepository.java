package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.LoginDataSource;
import com.jmt.sg.sgtareas.data.datasource.LoginDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.RouteGeolocationDataSource;
import com.jmt.sg.sgtareas.data.datasource.RouteGeolocationDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgtareas.domain.interactor.LoginInteractor;
import com.jmt.sg.sgtareas.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.LoginRepository;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.RouteGeolocationRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteGeolocationDataRepository implements RouteGeolocationRepository {

    private static final String TAG = "LoginDataRepository";
    private final RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory;

    public RouteGeolocationDataRepository(RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory) {
        this.routeGeolocationDataSourceFactory = routeGeolocationDataSourceFactory;
    }

    @Override
    public void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback) {
        RouteGeolocationDataSource geolocationDataSource = routeGeolocationDataSourceFactory.create(DataSourceFactory.CLOUD);
        geolocationDataSource.storeGeolocation(lat, lng, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                rCallback.onRegisterGeolocationSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                rCallback.onRegisterGeolocationError(message);
            }
        });
    }
}