package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.data.repository.SGInfoDataRepository;
import com.jmt.sg.sgtareas.data.model.Trackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public class SGInfoInteractor {
    private final SGInfoDataRepository sgInfoDataRepository;

    public SGInfoInteractor(SGInfoDataRepository sgInfoDataRepository) {
        this.sgInfoDataRepository = sgInfoDataRepository;
    }

    public void saveTrackableType(Trackeable trackeable) {
        sgInfoDataRepository.saveTrackableType(trackeable);
    }


    public Trackeable getTrackableType() {
        return  sgInfoDataRepository.getTrackableType();
    }
}