package com.jmt.sg.sgtareas.data.model;

import com.jmt.sg.sgtareas.data.mapper.BaseResponse;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class ValidateOrganizationResponse extends BaseResponse {
    private String message;
    private OrganizationEntity data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrganizationEntity getData() {
        return data;
    }

    public void setData(OrganizationEntity data) {
        this.data = data;
    }
}