package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.domain.repository.VisitCheckRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class VisitCheckInteractor {
    private final VisitCheckRepository visitCheckRepository;

    public VisitCheckInteractor(VisitCheckRepository visitCheckRepository) {
        this.visitCheckRepository = visitCheckRepository;
    }

    public void checkListCheck(long checklist_id, final Callback callback){
        visitCheckRepository.checkListCheck(checklist_id,callback);
    }

    public void uncheckListCheck(long checklist_id, final Callback callback){
        visitCheckRepository.uncheckListCheck(checklist_id,callback);
    }

    public interface Callback {
        void onCheckListSuccess();
        void onCheckListError(String message);
    }
}