package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.VisitCheckInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface VisitCheckRepository {
    void checkListCheck(long checklist_id, final VisitCheckInteractor.Callback visitCheckInteractorCallback);
    void uncheckListCheck(long checklist_id, final VisitCheckInteractor.Callback visitCheckInteractorCallback);
}