package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.TrackerRemoveInteractor;
import com.jmt.sg.sgtareas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerRemoveDataRepository {
    void removeVehicleData(TrackerRemoveInteractor.Callback vCallback);
    void removeAgentData(TrackerRemoveInteractor.Callback aCallback);
}