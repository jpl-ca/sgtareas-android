package com.jmt.sg.sgtareas.data.model;

import com.jmt.sg.sgtareas.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/16/16.
 */
public class VisitCheckListResponse extends BaseResponse {
    private long id;

    private String description;

    private int checked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}