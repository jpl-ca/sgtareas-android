package com.jmt.sg.sgtareas.data.datasource.rest.service;

import android.content.Context;

import com.google.gson.JsonObject;
import com.jmt.sg.sgtareas.data.datasource.rest.api.ApiService;
import com.jmt.sg.sgtareas.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgtareas.data.model.RouteResponse;

import org.json.JSONObject;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class RouteVisitService extends ApiService{
    private RouteVisitApi apiService;

    public RouteVisitService(Context context) {
        super(context);
        apiService = retrofit.create(RouteVisitApi.class);
    }

    public RouteVisitApi getApi() {
        return apiService;
    }

    public interface RouteVisitApi{
        @GET(_api+"/auth/me/tasks")
        Observable<RouteResponse> getCurrentRoute();

        @FormUrlEncoded
        @POST(_api+"/auth/me/check-list-item")
        Observable<JSONObject> checkListCheck(@Field("id") long checklist_id);

        @FormUrlEncoded
        @POST(_api+"/auth/me/uncheck-list-item")
        Observable<JSONObject> uncheckListCheck(@Field("id") long checklist_id);

        @FormUrlEncoded
        @POST(_api+"/auth/me/reschedule-visit-point")
        Observable<JSONObject> rescheduleVisitPoints(@Field("id") long visit_point_id,@Field("comment_of_rescheduling") String comment_of_rescheduling);
    }
}