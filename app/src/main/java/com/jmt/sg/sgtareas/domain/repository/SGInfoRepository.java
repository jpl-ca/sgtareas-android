package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.data.model.Trackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SGInfoRepository {
    void saveTrackableType(Trackeable trackeable);
    Trackeable getTrackableType();
}