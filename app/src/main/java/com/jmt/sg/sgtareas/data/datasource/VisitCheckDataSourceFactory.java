package com.jmt.sg.sgtareas.data.datasource;

import android.content.Context;

import com.jmt.sg.sgtareas.data.datasource.rest.RestVisitCheckListDataSource;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class VisitCheckDataSourceFactory {
    private final Context context;

    public VisitCheckDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public VisitCheckDataSource create(DataSourceFactory dataSource) {
        VisitCheckDataSource visitCheckDataSource = null;
        switch (dataSource) {
            case CLOUD:
                visitCheckDataSource = new RestVisitCheckListDataSource(context);
        }
        return visitCheckDataSource;
    }
}