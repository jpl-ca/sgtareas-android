package com.jmt.sg.sgtareas.data.datasource;

import com.jmt.sg.sgtareas.data.model.Trackeable;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SGInfoDataSource {
    void saveTrackeableType(Trackeable trackeable);
    Trackeable getTrackeableType();
}