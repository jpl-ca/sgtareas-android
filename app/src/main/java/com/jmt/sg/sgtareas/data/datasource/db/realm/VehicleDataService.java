package com.jmt.sg.sgtareas.data.datasource.db.realm;

import android.content.Context;

import com.jmt.sg.sgtareas.data.datasource.db.entity.VehicleTrackeableRealm;
import com.jmt.sg.sgtareas.data.datasource.db.rx.RealmObservable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

import io.realm.Realm;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by jmtech on 5/16/16.
 */
public class VehicleDataService implements VehicleDataServiceI{

    private final Context context;

    public VehicleDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<Boolean> registerVehicle(final VehicleTrackeable vehicleTrackeable) {
        return RealmObservable.object(context, new Func1<Realm, VehicleTrackeableRealm>() {
            @Override
            public VehicleTrackeableRealm call(Realm realm) {
                VehicleTrackeableRealm vehicleR = new VehicleTrackeableRealm();
                vehicleR.setId(vehicleTrackeable.getId());

                vehicleR.setBrand(vehicleTrackeable.getBrand());
                vehicleR.setColor(vehicleTrackeable.getColor());
                vehicleR.setGas_consumption_rate(vehicleTrackeable.getGas_consumption_rate());
                vehicleR.setManufacture_year(vehicleTrackeable.getManufacture_year());
                vehicleR.setModel(vehicleTrackeable.getModel());
                vehicleR.setPlate(vehicleTrackeable.getPlate());
                vehicleR.setLocation_frequency(vehicleTrackeable.getLocation_frequency());

                vehicleR.setType(vehicleTrackeable.getType());
                vehicleR.setToken(vehicleTrackeable.getToken());
                vehicleR.setOrganization_id(vehicleTrackeable.getOrganization_id());

                vehicleR.setLocation_frequency(vehicleTrackeable.getLocation_frequency());
                vehicleR = realm.copyToRealmOrUpdate(vehicleR);
                return vehicleR;
            }
        }).map(new Func1<VehicleTrackeableRealm, Boolean>() {
            @Override
            public Boolean call(VehicleTrackeableRealm vehicleR) {
                return vehicleR != null;
            }
        });
    }

    @Override
    public Observable<VehicleTrackeable> getVehicle() {
        return RealmObservable.object(context, new Func1<Realm, VehicleTrackeableRealm>() {
            @Override
            public VehicleTrackeableRealm call(Realm realm) {
                VehicleTrackeableRealm agentR = realm.where(VehicleTrackeableRealm.class).findFirst();
                return agentR;
            }
        }).map(new Func1<VehicleTrackeableRealm, VehicleTrackeable>() {
            @Override
            public VehicleTrackeable call(VehicleTrackeableRealm vehicleR) {
                VehicleTrackeable vehicle = new VehicleTrackeable();
                vehicle.setId(vehicleR.getId());

                vehicle.setBrand(vehicleR.getBrand());
                vehicle.setColor(vehicleR.getColor());
                vehicle.setGas_consumption_rate(vehicleR.getGas_consumption_rate());
                vehicle.setManufacture_year(vehicleR.getManufacture_year());
                vehicle.setModel(vehicleR.getModel());
                vehicle.setPlate(vehicleR.getPlate());
                vehicle.setLocation_frequency(vehicleR.getLocation_frequency());
                vehicle.setType(vehicleR.getType());
                vehicle.setToken(vehicleR.getToken());
                vehicle.setOrganization_id(vehicleR.getOrganization_id());

                vehicle.setLocation_frequency(vehicleR.getLocation_frequency());
                return vehicle;
            }
        });
    }

    @Override
    public Observable<Boolean> removeVehicle() {
        return RealmObservable.object(context, new Func1<Realm, VehicleTrackeableRealm>() {
            @Override
            public VehicleTrackeableRealm call(Realm realm) {
                VehicleTrackeableRealm us = realm.where(VehicleTrackeableRealm.class).findFirst();
                realm.where(VehicleTrackeableRealm.class).findAll().clear();
                return us;
            }
        }).map(new Func1<VehicleTrackeableRealm, Boolean>() {
            @Override
            public Boolean call(VehicleTrackeableRealm realmUser) {
                return true;
            }
        });
    }
}
