package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.TrackerSaveDataRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataSaveRepository implements TrackerSaveDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public TrackerDataSaveRepository(TrackeableDataSourceFactory trackeableDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public void saveTrackerData(VehicleTrackeable vTrackeable, final TrackerSaveInteractor.Callback vCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.saveVehicle(vTrackeable, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                System.out.println("Datos Gaurdados........Veh");
                vCallback.onTrackerDataSaved();
            }

            @Override
            public void onError(Object object) {
                System.out.println("Datos Gaurdados........Agent");
                String mess = "";
                vCallback.onTrackerDataSavedError(mess);
            }
        });
    }

    @Override
    public void saveTrackerData(AgentTrackeable aTrackeable, final TrackerSaveInteractor.Callback aCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.saveAgent(aTrackeable, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                aCallback.onTrackerDataSaved();
            }

            @Override
            public void onError(Object object) {
                String mess = "";
                aCallback.onTrackerDataSavedError(mess);
            }
        });
    }
}