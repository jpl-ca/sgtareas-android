package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.VisitCheckDataSource;
import com.jmt.sg.sgtareas.data.datasource.VisitCheckDataSourceFactory;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.domain.interactor.VisitCheckInteractor;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.VisitCheckRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class VisitCheckDataRepository implements VisitCheckRepository {

    private static final String TAG = "LoginDataRepository";
    private final VisitCheckDataSourceFactory visitCheckDataSourceFactory;

    public VisitCheckDataRepository(VisitCheckDataSourceFactory visitCheckDataSourceFactory) {
        this.visitCheckDataSourceFactory = visitCheckDataSourceFactory;
    }

    @Override
    public void checkListCheck(long checklist_id,final VisitCheckInteractor.Callback visitCheckInteractorCallback) {
        VisitCheckDataSource visitCheckDataSource = visitCheckDataSourceFactory.create(DataSourceFactory.CLOUD);
        visitCheckDataSource.checkListCheck(checklist_id, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                visitCheckInteractorCallback.onCheckListSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                visitCheckInteractorCallback.onCheckListError(message);
            }
        });
    }

    @Override
    public void uncheckListCheck(long checklist_id,final VisitCheckInteractor.Callback visitCheckInteractorCallback) {
        VisitCheckDataSource visitCheckDataSource = visitCheckDataSourceFactory.create(DataSourceFactory.CLOUD);
        visitCheckDataSource.uncheckListCheck(checklist_id, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                visitCheckInteractorCallback.onCheckListSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                visitCheckInteractorCallback.onCheckListError(message);
            }
        });
    }
}