package com.jmt.sg.sgtareas.data.datasource;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface TrackeableDataSource {
    void saveAgent(AgentTrackeable agentTrackeable, RepositoryCallback repositoryCallback);
    void saveVehicle(VehicleTrackeable vehicleTrackeable, RepositoryCallback repositoryCallback);

    void getAgent(RepositoryCallback repositoryCallback);
    void getVehicle(RepositoryCallback repositoryCallback);

    void removeAgent(RepositoryCallback repositoryCallback);
    void removeVehicle(RepositoryCallback repositoryCallback);
}