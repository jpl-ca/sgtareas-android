package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.RescheduleVisitInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RescheduleVisitRepository {
    void rescheduleVisit(long visit_point_id, String comment, final RescheduleVisitInteractor.Callback loginIteractorCallback);
}