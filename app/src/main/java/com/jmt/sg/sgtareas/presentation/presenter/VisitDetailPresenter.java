package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;
import android.location.Location;
import com.google.android.gms.maps.model.LatLng;

import com.jmt.sg.sgtareas.data.datasource.RescheduleVisitDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.VisitCheckDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.rest.api.S;
import com.jmt.sg.sgtareas.data.repository.RescheduleVisitDataRepository;
import com.jmt.sg.sgtareas.data.repository.VisitCheckDataRepository;
import com.jmt.sg.sgtareas.domain.interactor.RescheduleVisitInteractor;
import com.jmt.sg.sgtareas.domain.interactor.VisitCheckInteractor;
import com.jmt.sg.sgtareas.domain.model.RouteVisitE;
import com.jmt.sg.sgtareas.domain.model.VisitCheckListE;
import com.jmt.sg.sgtareas.domain.repository.RescheduleVisitRepository;
import com.jmt.sg.sgtareas.presentation.ui.fragment.VisitDetailFragment;
import com.jmt.sg.sgtareas.presentation.utils.Constants;
import com.jmt.sg.sgtareas.presentation.view.VisitDetailView;
import com.jmt.sg.sgtareas.tracking.invoker.GpsInvoker;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public class VisitDetailPresenter implements Presenter<VisitDetailView>, VisitCheckInteractor.Callback{
    private Context context;
    private VisitDetailView visitDetailView;
    private RouteVisitE routeVisit;

    private GpsInvoker gpsInvoker;

    private VisitCheckInteractor visitCheckInteractor;

    private RescheduleVisitInteractor rescheduleVisitInteractor;

    public VisitDetailPresenter(){
        routeVisit = new RouteVisitE();
    }

    @Override
    public void addView(VisitDetailView view) {
        visitDetailView = view;
        context = view.getContext();
        routeVisit = (RouteVisitE) visitDetailView.getAppActivity().getIntent().getSerializableExtra(VisitDetailFragment.ROUTE_VISIT_DATA);
        visitDetailView.settingData(routeVisit);

        gpsInvoker = new GpsInvoker(context);

        VisitCheckDataRepository visitCheckDataRepository = new VisitCheckDataRepository(new VisitCheckDataSourceFactory(context));
        visitCheckInteractor = new VisitCheckInteractor(visitCheckDataRepository);

        RescheduleVisitRepository rescheduleVisitRepository = new RescheduleVisitDataRepository(new RescheduleVisitDataSourceFactory(context));
        rescheduleVisitInteractor = new RescheduleVisitInteractor(rescheduleVisitRepository);
    }

    @Override
    public void removeView() {
        visitDetailView = null;
    }

    public ArrayList<VisitCheckListE> getCheckList() {
        return routeVisit.getChecklist();
    }

    public RouteVisitE getRouteVisit() {
        return routeVisit;
    }

    public void updateCheckListStatus(long checkListId, int checkStatus) {
        if(checkStatus == Constants.CHECKED_LIST_STATE.Checked)
             visitCheckInteractor.checkListCheck(checkListId, this);
        else visitCheckInteractor.uncheckListCheck(checkListId, this);
    }
    public void rescheduleVisitPoint(final long visit_point_id,String comment){
        rescheduleVisitInteractor.rescheduleVisitPoint(visit_point_id, comment, new RescheduleVisitInteractor.Callback() {
            @Override
            public void onRescheduleSuccess() {
                changeVisitPointStateLocal(visit_point_id, Constants.VISIT_STATE.ReScheduling);
            }
            @Override
            public void onRescheduleError(String message) {

            }
        });
    }


    public void changeVisitPointStateLocal(long visit_point_id, final int new_state){
        routeVisit.setVisit_state_id(new_state);
        visitDetailView.onChangeVisitStateSuccess(new_state);
    }

    public void getMyLocation(final onGetMyLocation callback) {
        gpsInvoker.startGpsData();
        gpsInvoker.startGpsListener(new GpsInvoker.LocationCallback() {
            @Override
            public void updateLocation(Location location) {
                callback.setMyLocation(new LatLng(location.getLatitude(),location.getLongitude()));
                gpsInvoker.stopGpsListener();
                gpsInvoker.stopGpsData();
            }
        });
    }

    @Override
    public void onCheckListSuccess() {
        visitDetailView.onCheckedSuccess();
    }

    @Override
    public void onCheckListError(String message) {
        visitDetailView.onRequestError(message);
    }

    public void updateCheckListState(int idx, int unchecked) {
        routeVisit.getChecklist().get(idx).setChecked(unchecked);
    }

    public interface onGetMyLocation{
        void setMyLocation(LatLng ll);
    }
}