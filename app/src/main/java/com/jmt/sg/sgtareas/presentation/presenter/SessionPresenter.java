package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;
import android.os.Handler;

import com.jmt.sg.sgtareas.data.datasource.SessionDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.repository.SessionDataRepository;
import com.jmt.sg.sgtareas.domain.interactor.SessionInteractor;
import com.jmt.sg.sgtareas.domain.repository.SessionRepository;
import com.jmt.sg.sgtareas.presentation.view.SessionView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by jmtech on 5/12/16.
 */
public class SessionPresenter implements Presenter<SessionView>,SessionInteractor.CheckSessionCallback {

    Context context;
    SessionView sessionView;
    private SessionInteractor sessionInteractor;

    @Override
    public void addView(SessionView view) {
        sessionView = view;
        context = view.getContext();
        SessionRepository sessionRepository = new SessionDataRepository(new SessionDataSourceFactory(context),new TrackeableDataMapper());
        sessionInteractor = new SessionInteractor(sessionRepository);
    }

    @Override
    public void removeView() {
        sessionView = null;
    }

    public boolean IsCasiPalindromo(String str){
        for (int i=0;i<str.length()/2;i++)
            if(str.charAt(i)!=str.charAt(str.length()-i-1))return false;
        return true;
    }

    public int numMasPopular(int[] array,int size){
        HashMap<Integer,Integer> numMap = new HashMap<>();
        for (int n : array) {
            int tot = 1;
            if(numMap.containsKey(n)){
                tot += numMap.get(n);
            }
            numMap.put(n,tot);
        }
        Set<Integer> numsK = numMap.keySet();
        ArrayList<Integer> numeros = new ArrayList<>();
        for (int n : numsK) {
            numeros.add(n);
        }

        Collections.sort(numeros);

        int max = -1;
        for (int n : numeros) {
            if(numMap.get(n)>max)max = numMap.get(n);
        }
        for (int n : numeros) {
            if(numMap.get(n)==max)return n;
        }
        return 0;
    }

    public void getSession(){
        sessionView.checking();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionInteractor.checkSession(SessionPresenter.this);
            }
        }, 1500);
    }

    @Override
    public void onValidateSuccess() {
        sessionView.hasSession(true);
    }

    @Override
    public void onValidateError(String message) {
        sessionView.hasSession(false);
        sessionView.showErrorMessage(message);
    }
}