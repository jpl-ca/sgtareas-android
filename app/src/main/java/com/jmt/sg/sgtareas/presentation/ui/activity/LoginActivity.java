package com.jmt.sg.sgtareas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.jmt.sg.sgtareas.presentation.ui.fragment.LoginFragment;
import com.jmt.sg.sgtareas.R;

public class LoginActivity extends BaseActivity implements LoginFragment.OnLoginListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this,MapHomeActivity.class));
        finish();
    }

    @Override
    public void validateOrganizationActivity() {
        startActivity(new Intent(this,OrganizationActivity.class));
        finish();
    }
}