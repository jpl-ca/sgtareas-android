package com.jmt.sg.sgtareas.data.datasource;

import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface LoginDataSource {
    void loginAgent(String identification_code, String password, RepositoryCallback repositoryCallback);
    void loginVehicle(String plate, String password, RepositoryCallback repositoryCallback);
}