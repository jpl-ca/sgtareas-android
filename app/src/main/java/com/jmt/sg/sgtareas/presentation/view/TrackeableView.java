package com.jmt.sg.sgtareas.presentation.view;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackeableView extends BaseView {
    void showData(VehicleTrackeable vehicleTrackeable);
    void showData(AgentTrackeable agentTrackeable);
    void showMessage(String message);
}