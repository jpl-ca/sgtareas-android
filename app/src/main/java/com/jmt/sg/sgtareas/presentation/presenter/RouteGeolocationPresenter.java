package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgtareas.data.datasource.LoginDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.RouteGeolocationDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.Trackeable;
import com.jmt.sg.sgtareas.data.repository.LoginDataRepository;
import com.jmt.sg.sgtareas.data.repository.RouteGeolocationDataRepository;
import com.jmt.sg.sgtareas.data.repository.SGInfoDataRepository;
import com.jmt.sg.sgtareas.data.repository.TrackerDataSaveRepository;
import com.jmt.sg.sgtareas.domain.interactor.LoginInteractor;
import com.jmt.sg.sgtareas.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgtareas.domain.interactor.SGInfoInteractor;
import com.jmt.sg.sgtareas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.LoginRepository;
import com.jmt.sg.sgtareas.domain.repository.TrackerSaveDataRepository;
import com.jmt.sg.sgtareas.presentation.view.LoginView;
import com.jmt.sg.sgtareas.presentation.view.RouteGeolocationView;

/**
 * Created by jmtech on 5/12/16.
 */
public class RouteGeolocationPresenter implements Presenter<RouteGeolocationView>, RouteGeolocationInteractor.Callback{

    Context context;
    RouteGeolocationView routeGeolocationView;
    private RouteGeolocationInteractor routeGeolocationInteractor;

    @Override
    public void addView(RouteGeolocationView view) {
        routeGeolocationView = view;
        context = routeGeolocationView.getContext();

        RouteGeolocationDataRepository routeGeolocationDataRepository = new RouteGeolocationDataRepository(new RouteGeolocationDataSourceFactory(context));
        routeGeolocationInteractor = new RouteGeolocationInteractor(routeGeolocationDataRepository);
    }

    @Override
    public void removeView() {
        routeGeolocationView = null;
    }

    public void registerRouteGeolocation(double lat, double lng){
        routeGeolocationInteractor.storeGeolocation(lat,lng,this);
    }

    @Override
    public void onRegisterGeolocationSuccess() {

    }

    @Override
    public void onRegisterGeolocationError(String message) {

    }
}