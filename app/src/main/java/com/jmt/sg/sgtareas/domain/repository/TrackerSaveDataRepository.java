package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerSaveDataRepository {
    void saveTrackerData(VehicleTrackeable vTrackeable, TrackerSaveInteractor.Callback vCallback);
    void saveTrackerData(AgentTrackeable aTrackeable, TrackerSaveInteractor.Callback aCallback);
}