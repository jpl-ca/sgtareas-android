package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.TrackerLoadDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerLoadInteractor {
    private final TrackerLoadDataRepository trackerRepository;

    public TrackerLoadInteractor(TrackerLoadDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void loadVehicleData(final Callback vCallback) {
        trackerRepository.loadVehicleData(vCallback);
    }

    public void loadAgentData(final Callback aCallback) {
        trackerRepository.loadAgentData(aCallback);
    }

    public interface Callback {
        void onLoadVehicleData(VehicleTrackeable vehicleTrackeable);
        void onLoadAgentData(AgentTrackeable agentTrackeable);
        void onLoadDataError(String message);
    }
}