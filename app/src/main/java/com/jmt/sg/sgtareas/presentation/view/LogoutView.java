package com.jmt.sg.sgtareas.presentation.view;

/**
 * Created by jmtech on 5/12/16.
 */
public interface LogoutView extends BaseView {
    void onLogout();
    void showErrorMessage(String message);
}