package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.LoginRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class LoginInteractor {
    private final LoginRepository loginRepository;

    public LoginInteractor(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public void loginAgent(String identification_code, String password, final Callback placeCallback) {
        loginRepository.loginAgent(identification_code, password, placeCallback);
    }

    public void loginVehicle(String plate, String password, final Callback placeCallback) {
        loginRepository.loginVehicle(plate, password, placeCallback);
    }

    public interface Callback {
        void onLoginSuccess(VehicleTrackeable vehicleTrackeable);
        void onLoginSuccess(AgentTrackeable agentTrackeable);
        void onLoginError(String message);
    }
}