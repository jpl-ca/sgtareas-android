package com.jmt.sg.sgtareas.domain.model;

import java.io.Serializable;

/**
 * Created by jmtech on 5/16/16.
 */
public class VisitCheckListE implements Serializable {
    private long id;

    private String description;

    private int checked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}