package com.jmt.sg.sgtareas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.domain.model.RouteVisitE;
import com.jmt.sg.sgtareas.presentation.ui.fragment.VisitDetailFragment;
import com.jmt.sg.sgtareas.presentation.utils.Constants;

import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitDetailActivity extends AppCompatActivity implements VisitDetailFragment.OnVisitDetailListener{

    public static final int CODE_RESULT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_detail);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void sendResultSuccess(RouteVisitE routeVisit) {
        Intent it = new Intent();
        it.putExtra(Constants.VISIT_STATE.VisitState,routeVisit);
        setResult(RESULT_OK,it);
    }
}