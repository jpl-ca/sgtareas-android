package com.jmt.sg.sgtareas.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.SessionDataSourceFactory;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.Trackeable;
import com.jmt.sg.sgtareas.data.repository.SGInfoDataRepository;
import com.jmt.sg.sgtareas.data.repository.SessionDataRepository;
import com.jmt.sg.sgtareas.data.repository.TrackerDataRemoveRepository;
import com.jmt.sg.sgtareas.domain.interactor.SGInfoInteractor;
import com.jmt.sg.sgtareas.domain.interactor.SessionInteractor;
import com.jmt.sg.sgtareas.domain.interactor.TrackerRemoveInteractor;
import com.jmt.sg.sgtareas.domain.repository.SessionRepository;
import com.jmt.sg.sgtareas.domain.repository.TrackerRemoveDataRepository;
import com.jmt.sg.sgtareas.presentation.view.LogoutView;
import com.jmt.sg.sgtareas.tracking.invoker.GpsInvoker;

/**
 * Created by jmtech on 5/12/16.
 */
public class LogoutPresenter implements Presenter<LogoutView>,SessionInteractor.CloseSessionCallback, TrackerRemoveInteractor.Callback {
    Context context;
    LogoutView logoutView;
    private SessionInteractor sessionInteractor;
    private TrackerRemoveInteractor trackerRemoveInteractor;
    private SGInfoInteractor sgInfoInteractor;
    private boolean REMOVED_FB,CLOSE_SESSION;

    private GpsInvoker gpsInvoker;

    @Override
    public void addView(LogoutView view) {
        logoutView = view;
        context = view.getContext();

        gpsInvoker = new GpsInvoker(context);
        gpsInvoker.stopGpsData();

        SessionRepository sessionRepository = new SessionDataRepository(new SessionDataSourceFactory(context),new TrackeableDataMapper());
        sessionInteractor = new SessionInteractor(sessionRepository);

        TrackerRemoveDataRepository trackerDataRemoveRepository = new TrackerDataRemoveRepository(new TrackeableDataSourceFactory(context));
        trackerRemoveInteractor = new TrackerRemoveInteractor(trackerDataRemoveRepository);

        SGInfoDataRepository sgInfoDataRepository = new SGInfoDataRepository(new SGInfoDataSourceFactory(context));
        sgInfoInteractor = new SGInfoInteractor(sgInfoDataRepository);
    }

    @Override
    public void removeView() {
        logoutView = null;
    }

    public void closeSession(){
        REMOVED_FB = false;
        CLOSE_SESSION = false;
        sessionInteractor.closeSession(this);
        removeUserData();
    }

    private void removeUserData() {
        if(sgInfoInteractor.getTrackableType() == Trackeable.VEHICLE)
            trackerRemoveInteractor.removeTrackerVehicleData(this);
        else trackerRemoveInteractor.removeTrackerAgentData(this);
    }

    @Override
    public void onLogoutSuccess() {
        CLOSE_SESSION = true;
        goToLogout();
    }

    @Override
    public void onLogoutError(String message) {
        CLOSE_SESSION = true;
        goToLogout();
    }

    private void goToLogout() {
        if(CLOSE_SESSION&&REMOVED_FB)
            logoutView.onLogout();
    }

    @Override
    public void onTrackerDataRemoved() {
        REMOVED_FB = true;
        goToLogout();
    }

    @Override
    public void onTrackerDataRemovedError(String mess) {
        REMOVED_FB = true;
        goToLogout();
    }
}