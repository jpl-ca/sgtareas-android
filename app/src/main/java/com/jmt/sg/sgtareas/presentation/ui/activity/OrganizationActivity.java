package com.jmt.sg.sgtareas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.presentation.ui.fragment.OrganizationFragment;

public class OrganizationActivity extends BaseActivity implements OrganizationFragment.OnOrganizationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }
}