package com.jmt.sg.sgtareas.presentation.utils.media;

import android.widget.ImageView;

/**
 * Created by jmtech on 5/12/16.
 */
public interface ImageLoader {
    void load(String url,ImageView imageView);
    void loadLocal(String path, ImageView imageView);
}