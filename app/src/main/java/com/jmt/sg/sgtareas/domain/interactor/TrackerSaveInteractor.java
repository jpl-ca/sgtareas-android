package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.TrackerSaveDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerSaveInteractor {
    private final TrackerSaveDataRepository trackerRepository;

    public TrackerSaveInteractor(TrackerSaveDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void saveTrackerData(VehicleTrackeable vTrackeable, final Callback vCallback) {
        trackerRepository.saveTrackerData(vTrackeable, vCallback);
    }

    public void saveTrackerData(AgentTrackeable aTrackeable, final Callback aCallback) {
        trackerRepository.saveTrackerData(aTrackeable, aCallback);
    }

    public interface Callback {
        void onTrackerDataSaved();
        void onTrackerDataSavedError(String mess);
    }

}