package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.TrackerLoadInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerLoadDataRepository {
    VehicleTrackeable loadVehicleData(TrackerLoadInteractor.Callback aCallback);
    AgentTrackeable loadAgentData(TrackerLoadInteractor.Callback aCallback);
}