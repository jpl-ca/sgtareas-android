package com.jmt.sg.sgtareas.presentation.view;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by jmtech on 5/12/16.
 */
public interface HomeMapView extends BaseView {
    void firstPosition(LatLng ll);
    void updatePosition(LatLng ll);
    Context getContext();
    Marker addVisitMarker(MarkerOptions markerOptions);
    void showMessage(String message);
    void clearMap();
}