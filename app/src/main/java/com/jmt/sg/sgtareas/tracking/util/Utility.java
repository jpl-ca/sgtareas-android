package com.jmt.sg.sgtareas.tracking.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.presentation.presenter.ConfirmDialogPresenter;
import com.jmt.sg.sgtareas.presentation.view.ConfirmDialogView;

/**
 * Created by jmtech on 7/5/16.
 */
public class Utility {

    public static final int REQUEST_CODE = 99;

    private static final String[] ACCESS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    public static boolean checkPermission(final Context context) {
        final Activity activity = (Activity) context;

        int currentAPIVersion = Build.VERSION.SDK_INT;

        if(currentAPIVersion>= Build.VERSION_CODES.M) {
            return checkPermissionM(activity);
        } else {
            return checkPermissionPreM(activity);
        }
    }

    private static boolean checkPermissionPreM(Context context) {
        String providerGps = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        boolean isProviderGps = providerGps.contains("gps");
        if(isProviderGps)return true;

        confirmCheckPermission(context);

        return false;
    }

    private static void confirmCheckPermission(final Context context) {
        ConfirmDialogPresenter confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) context).startActivityForResult(intent, REQUEST_CODE);
            }
            @Override
            public void clickConfirm(String input) {}
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showMessageDialog(context.getString(R.string.s_activar_gps));
    }

    private static boolean checkPermissionM(final Activity activity) {
        boolean needPermissionGranted = checkLocationPermission(activity);
        if (needPermissionGranted) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {

                ConfirmDialogPresenter confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
                    @Override
                    public void clickConfirm() {
                        ActivityCompat.requestPermissions(activity, ACCESS_LOCATION, REQUEST_CODE);
                    }
                    @Override
                    public void clickConfirm(String input) {}
                    @Override
                    public void clickCancel() {}
                    @Override
                    public Context getContext() {
                        return activity;
                    }
                });
                confirmDialogPresenter.showMessageDialog(activity.getString(R.string.s_activar_gps));

            } else {
                ActivityCompat.requestPermissions(activity, ACCESS_LOCATION, REQUEST_CODE);
            }
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkLocationPermission(Context context) {
        boolean needPermissionGranted = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        return needPermissionGranted;
    }
}
