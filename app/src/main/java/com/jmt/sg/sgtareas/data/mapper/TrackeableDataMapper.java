package com.jmt.sg.sgtareas.data.mapper;

import com.jmt.sg.sgtareas.data.model.AgentTrackeableEntity;
import com.jmt.sg.sgtareas.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgtareas.data.model.VehicleTrackeableEntity;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/16/16.
 */
public class TrackeableDataMapper {

    public VehicleTrackeable transformVehicle(VehicleTrackeableEntity trackeableEntity) {
        VehicleTrackeable vehicleTrackeable = new VehicleTrackeable();
        if(trackeableEntity==null)return vehicleTrackeable;

        vehicleTrackeable.setId(trackeableEntity.getId());
        vehicleTrackeable.setBrand(trackeableEntity.getBrand());
        vehicleTrackeable.setType(trackeableEntity.getType());
        vehicleTrackeable.setToken(trackeableEntity.getToken());
        vehicleTrackeable.setPlate(trackeableEntity.getPlate());
        vehicleTrackeable.setOrganization_id(trackeableEntity.getOrganization_id());
        vehicleTrackeable.setModel(trackeableEntity.getModel());
        vehicleTrackeable.setManufacture_year(trackeableEntity.getManufacture_year());
        vehicleTrackeable.setLocation_frequency(trackeableEntity.getLocation_frequency());
        vehicleTrackeable.setColor(trackeableEntity.getColor());
        vehicleTrackeable.setGas_consumption_rate(trackeableEntity.getGas_consumption_rate());
        return vehicleTrackeable;
    }

    public VehicleTrackeable transformVehicleResponse(LoginTrackeableResponse loginTrackeableResponse) {
        VehicleTrackeable vehicleTrackeable = new VehicleTrackeable();
        if(loginTrackeableResponse ==null)return vehicleTrackeable;

        vehicleTrackeable.setId(loginTrackeableResponse.getId());
        vehicleTrackeable.setBrand(loginTrackeableResponse.getBrand());
        vehicleTrackeable.setType(loginTrackeableResponse.getType());
        vehicleTrackeable.setToken(loginTrackeableResponse.getToken());
        vehicleTrackeable.setPlate(loginTrackeableResponse.getPlate());
        vehicleTrackeable.setOrganization_id(loginTrackeableResponse.getOrganization_id());
        vehicleTrackeable.setModel(loginTrackeableResponse.getModel());
        vehicleTrackeable.setManufacture_year(loginTrackeableResponse.getManufacture_year());
        vehicleTrackeable.setLocation_frequency(loginTrackeableResponse.getLocation_frequency());
        vehicleTrackeable.setColor(loginTrackeableResponse.getColor());
        vehicleTrackeable.setGas_consumption_rate(loginTrackeableResponse.getGas_consumption_rate());
        return vehicleTrackeable;
    }


    public AgentTrackeable transformAgent(AgentTrackeableEntity agentTrackeableEntity) {
        AgentTrackeable agentTrackeable = new AgentTrackeable();
        if(agentTrackeableEntity==null)return agentTrackeable;

        agentTrackeable.setId(agentTrackeableEntity.getId());
        agentTrackeable.setAuthentication_code(agentTrackeableEntity.getAuthentication_code());
        agentTrackeable.setFirst_name(agentTrackeableEntity.getFirst_name());
        agentTrackeable.setLast_name(agentTrackeableEntity.getLast_name());
        agentTrackeable.setType(agentTrackeableEntity.getType());
        agentTrackeable.setToken(agentTrackeableEntity.getToken());
        agentTrackeable.setOrganization_id(agentTrackeableEntity.getOrganization_id());
        agentTrackeable.setLocation_frequency(agentTrackeableEntity.getLocation_frequency());
        return agentTrackeable;
    }

    public AgentTrackeable transformAgentResponse(LoginTrackeableResponse loginTrackeableResponse) {
        AgentTrackeable agentTrackeable = new AgentTrackeable();
        if(loginTrackeableResponse ==null)return agentTrackeable;

        agentTrackeable.setId(loginTrackeableResponse.getId());
        agentTrackeable.setAuthentication_code(loginTrackeableResponse.getAuthentication_code());
        agentTrackeable.setFirst_name(loginTrackeableResponse.getFirst_name());
        agentTrackeable.setLast_name(loginTrackeableResponse.getLast_name());
        agentTrackeable.setType(loginTrackeableResponse.getType());
        agentTrackeable.setToken(loginTrackeableResponse.getToken());
        agentTrackeable.setOrganization_id(loginTrackeableResponse.getOrganization_id());
        agentTrackeable.setLocation_frequency(loginTrackeableResponse.getLocation_frequency());
        return agentTrackeable;
    }

}