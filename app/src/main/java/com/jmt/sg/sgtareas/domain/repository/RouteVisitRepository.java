package com.jmt.sg.sgtareas.domain.repository;

import com.jmt.sg.sgtareas.domain.interactor.RouteVisitInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RouteVisitRepository {
    void getRouteVisit(final RouteVisitInteractor.Callback rouInteractorCallback);
}