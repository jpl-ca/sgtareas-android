package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSource;
import com.jmt.sg.sgtareas.data.datasource.SGInfoDataSourceFactory;
import com.jmt.sg.sgtareas.data.model.Trackeable;
import com.jmt.sg.sgtareas.domain.repository.SGInfoRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class SGInfoDataRepository implements SGInfoRepository {
    private final SGInfoDataSourceFactory sgInfoDataSourceFactory;

    public SGInfoDataRepository(SGInfoDataSourceFactory sgInfoDataSourceFactory) {
        this.sgInfoDataSourceFactory = sgInfoDataSourceFactory;
    }

    @Override
    public void saveTrackableType(Trackeable trackeable) {
        SGInfoDataSource sgInfoDataSource = sgInfoDataSourceFactory.create(SGInfoDataSourceFactory.PREFERENCES);
        sgInfoDataSource.saveTrackeableType(trackeable);
    }

    @Override
    public Trackeable getTrackableType() {
        SGInfoDataSource sgInfoDataSource = sgInfoDataSourceFactory.create(SGInfoDataSourceFactory.PREFERENCES);
        return sgInfoDataSource.getTrackeableType();
    }
}