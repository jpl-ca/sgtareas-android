package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.domain.interactor.TrackerLoadInteractor;
import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.TrackerLoadDataRepository;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataLoadRepository implements TrackerLoadDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public TrackerDataLoadRepository(TrackeableDataSourceFactory trackeableDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public VehicleTrackeable loadVehicleData(final TrackerLoadInteractor.Callback vCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.getVehicle(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                VehicleTrackeable vt = (VehicleTrackeable)object;
                vCallback.onLoadVehicleData(vt);
            }

            @Override
            public void onError(Object object) {
                String mess = object.toString();
                vCallback.onLoadDataError(mess);
            }
        });
        return null;
    }

    @Override
    public AgentTrackeable loadAgentData(final TrackerLoadInteractor.Callback aCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.getAgent(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                AgentTrackeable at = (AgentTrackeable)object;
                aCallback.onLoadAgentData(at);
            }

            @Override
            public void onError(Object object) {
                String mess = object.toString();
                aCallback.onLoadDataError(mess);
            }
        });
        return null;
    }
}