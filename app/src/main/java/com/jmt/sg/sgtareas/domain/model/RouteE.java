package com.jmt.sg.sgtareas.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteE implements Serializable{
    private long id;

    private ArrayList<RouteVisitE> visit_points;

    private String description;

    private String start_date;

    private String end_date;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<RouteVisitE> getVisit_points() {
        return visit_points;
    }

    public void setVisit_points(ArrayList<RouteVisitE> visit_points) {
        this.visit_points = visit_points;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}