package com.jmt.sg.sgtareas.data.datasource.db.realm;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface VehicleDataServiceI {
    Observable<VehicleTrackeable> getVehicle();
    Observable<Boolean> registerVehicle(VehicleTrackeable user);
    Observable<Boolean> removeVehicle();
}