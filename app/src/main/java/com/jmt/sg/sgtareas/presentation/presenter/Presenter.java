package com.jmt.sg.sgtareas.presentation.presenter;

/**
 * Created by jmtech on 5/12/16.
 */
public interface Presenter<T> {
    void addView(T view);
    void removeView();
}