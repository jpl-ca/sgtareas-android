package com.jmt.sg.sgtareas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.presentation.ui.fragment.OrganizationFragment;
import com.jmt.sg.sgtareas.presentation.ui.fragment.SplashFragment;

public class SplashActivity extends BaseActivity implements SplashFragment.OnSplashListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void toLoginActivity() {
        startActivity(new Intent(this,OrganizationActivity.class));
        finish();
    }

    @Override
    public void toHomeActivity() {
        startActivity(new Intent(this,MapHomeActivity.class));
        finish();
    }
}