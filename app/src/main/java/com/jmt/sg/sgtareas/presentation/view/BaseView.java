package com.jmt.sg.sgtareas.presentation.view;

import android.content.Context;

/**
 * Created by jmtech on 5/12/16.
 */
public interface BaseView {
    Context getContext();
}