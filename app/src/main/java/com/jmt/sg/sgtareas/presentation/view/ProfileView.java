package com.jmt.sg.sgtareas.presentation.view;

import com.jmt.sg.sgtareas.domain.model.AgentTrackeable;
import com.jmt.sg.sgtareas.domain.model.VehicleTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface ProfileView extends BaseView {
    void showAgentInfo(AgentTrackeable agentTrackeable);
    void showVehicleInfo(VehicleTrackeable vehicleTrackeable);
    void showErrorMessage(String message);
}