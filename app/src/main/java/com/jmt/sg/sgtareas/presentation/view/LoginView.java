package com.jmt.sg.sgtareas.presentation.view;

/**
 * Created by jmtech on 5/12/16.
 */
public interface LoginView extends BaseView {
    void showLoading();
    void hideLoading();
    void successLogin();
    void showErrorMessage(String message);
}