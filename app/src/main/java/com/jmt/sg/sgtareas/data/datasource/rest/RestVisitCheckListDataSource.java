package com.jmt.sg.sgtareas.data.datasource.rest;

import android.content.Context;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.data.datasource.VisitCheckDataSource;
import com.jmt.sg.sgtareas.data.datasource.preferences.JMStore;
import com.jmt.sg.sgtareas.data.datasource.rest.service.RouteVisitService;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;

import retrofit.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jmtech on 5/13/16.
 */
public class RestVisitCheckListDataSource implements VisitCheckDataSource {

    private CompositeSubscription compositeSubscription;
    private RouteVisitService routeVisitService;
    private final Context context;
    private final JMStore jmStore;

    public RestVisitCheckListDataSource(Context context) {
        this.context= context;
        routeVisitService = new RouteVisitService(context);
        compositeSubscription = new CompositeSubscription();
        jmStore = new JMStore(context);
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    @Override
    public void checkListCheck(final long checklist_id, final RepositoryCallback repositoryCallback){
        Observable<JSONObject> checkObservable = routeVisitService.getApi().checkListCheck(checklist_id);

        Subscription subscription = checkObservable.
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject b) {
                                System.out.println("checkListCheck()--->" + b);
                                repositoryCallback.onSuccess(b);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(context.getString(R.string.s_error_de_conexion));
                                    repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        repositoryCallback.onError(str);
                                    } catch (IOException e) {
                                        repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                    }
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    @Override
    public void uncheckListCheck(final long checklist_id, final RepositoryCallback repositoryCallback){
        Observable<JSONObject> checkObservable = routeVisitService.getApi().uncheckListCheck(checklist_id);

        Subscription subscription = checkObservable.
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject b) {
                                System.out.println("checkListCheck()--->" + b);
                                repositoryCallback.onSuccess(b);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(context.getString(R.string.s_error_de_conexion));
                                    repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        repositoryCallback.onError(str);
                                    } catch (IOException e) {
                                        repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                    }
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}