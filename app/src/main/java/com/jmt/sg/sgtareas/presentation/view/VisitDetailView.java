package com.jmt.sg.sgtareas.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jmt.sg.sgtareas.domain.model.RouteVisitE;

/**
 * Created by jmtech on 5/12/16.
 */
public interface VisitDetailView extends BaseView {
    Context getContext();
    AppCompatActivity getAppActivity();

    void settingData(RouteVisitE routeVisitE);

    void onChangeVisitStateSuccess(int new_state);
    void onCheckedSuccess();
    void onRequestError(String message);
}