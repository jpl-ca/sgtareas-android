package com.jmt.sg.sgtareas.data.model;

/**
 * Created by jmtech on 5/18/16.
 */
public enum Trackeable {
    AGENT,
    VEHICLE,
    NONE
}