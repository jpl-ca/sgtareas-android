package com.jmt.sg.sgtareas.data.datasource;

import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface VisitCheckDataSource {
    void checkListCheck(long checklist_id, final RepositoryCallback repositoryCallback);
    void uncheckListCheck(long checklist_id, final RepositoryCallback repositoryCallback);
}