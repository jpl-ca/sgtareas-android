package com.jmt.sg.sgtareas.domain.repository;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RepositoryCallback {
    void onError(Object object);
    void onSuccess(Object object);
}