package com.jmt.sg.sgtareas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgtareas.R;
import com.jmt.sg.sgtareas.domain.model.RouteVisitE;
import com.jmt.sg.sgtareas.presentation.ui.fragment.MapHomeFragment;
import com.jmt.sg.sgtareas.presentation.ui.fragment.TimelineFragment;
import com.jmt.sg.sgtareas.presentation.ui.fragment.VisitDetailFragment;
import com.jmt.sg.sgtareas.tracking.util.Utility;

import java.util.ArrayList;

public class MapHomeActivity extends BaseActivity implements MapHomeFragment.OnHomeMapListener {
    private final MapHomeFragment mapHomeFragment = MapHomeFragment.instance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_home);
        showFragment();
    }

    private void showFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mapHomeFragment).commit();
    }

    @Override
    public void profileActivity() {
        Intent it = new Intent(this,ProfileActivity.class);
        startActivity(it);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Utility.REQUEST_CODE : mapHomeFragment.onRequestPermissionsResult(requestCode,resultCode);break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.REQUEST_CODE : mapHomeFragment.onRequestPermissionsResult(requestCode,permissions,grantResults); break;
        }
    }

    @Override
    public void visitDetailActivity(RouteVisitE routeVisit) {
        Intent it = new Intent(this,VisitDetailActivity.class);
        it.putExtra(VisitDetailFragment.ROUTE_VISIT_DATA,routeVisit);
        startActivityForResult(it,VisitDetailActivity.CODE_RESULT);
    }

    @Override
    public void timeLineActivity(ArrayList<RouteVisitE> routeVisits) {
        Intent it = new Intent(this,TimelineActivity.class);
        it.putExtra(TimelineFragment.ROUTE_VISITS_DATA,routeVisits);
        startActivityForResult(it,VisitDetailActivity.CODE_RESULT);
    }
}