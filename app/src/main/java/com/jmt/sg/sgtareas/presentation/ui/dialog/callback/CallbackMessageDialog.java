package com.jmt.sg.sgtareas.presentation.ui.dialog.callback;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface CallbackMessageDialog {
    void finish();
}