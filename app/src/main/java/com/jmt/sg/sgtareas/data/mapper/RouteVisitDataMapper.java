package com.jmt.sg.sgtareas.data.mapper;

import com.google.gson.Gson;
import com.jmt.sg.sgtareas.data.model.RouteResponse;
import com.jmt.sg.sgtareas.domain.model.RouteE;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteVisitDataMapper {
    Gson gson;

    public RouteVisitDataMapper(Gson gson){
        this.gson = gson;
    }

    public RouteE transformRoute(RouteResponse routeResponse){
        String routeStr = gson.toJson(routeResponse);
        RouteE route = gson.fromJson(routeStr,RouteE.class);
        return route;
    }
}