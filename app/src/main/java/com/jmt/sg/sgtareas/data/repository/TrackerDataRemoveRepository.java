package com.jmt.sg.sgtareas.data.repository;

import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgtareas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgtareas.data.model.DataSourceFactory;
import com.jmt.sg.sgtareas.domain.interactor.TrackerRemoveInteractor;
import com.jmt.sg.sgtareas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgtareas.domain.repository.TrackerRemoveDataRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataRemoveRepository implements TrackerRemoveDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;

    public TrackerDataRemoveRepository(TrackeableDataSourceFactory trackeableDataSourceFactory) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
    }

    @Override
    public void removeVehicleData(final TrackerRemoveInteractor.Callback vCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.removeVehicle(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                vCallback.onTrackerDataRemoved();
            }

            @Override
            public void onError(Object object) {
                String mess = object.toString();
                vCallback.onTrackerDataRemovedError(mess);
            }
        });
    }

    @Override
    public void removeAgentData(final TrackerRemoveInteractor.Callback aCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.removeAgent(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                aCallback.onTrackerDataRemoved();
            }

            @Override
            public void onError(Object object) {
                String mess = object.toString();
                aCallback.onTrackerDataRemovedError(mess);
            }
        });
    }
}