package com.jmt.sg.sgtareas.domain.interactor;

import com.jmt.sg.sgtareas.domain.repository.TrackerRemoveDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerRemoveInteractor {
    private final TrackerRemoveDataRepository trackerRepository;

    public TrackerRemoveInteractor(TrackerRemoveDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void removeTrackerAgentData(final Callback vCallback) {
        trackerRepository.removeAgentData(vCallback);
    }

    public void removeTrackerVehicleData(final Callback aCallback) {
        trackerRepository.removeVehicleData(aCallback);
    }

    public interface Callback {
        void onTrackerDataRemoved();
        void onTrackerDataRemovedError(String mess);
    }

}