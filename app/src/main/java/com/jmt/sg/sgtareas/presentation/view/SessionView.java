package com.jmt.sg.sgtareas.presentation.view;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SessionView extends BaseView {
    void checking();
    void hasSession(boolean b);
    void showErrorMessage(String message);
}